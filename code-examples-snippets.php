<?php

/*
Plugin Name: Code Examples Snippets
Plugin URI: http://paolofalomo.it
Description: Code Snippets
Version: 0.1
Author: paolo
Author URI: https://profiles.wordpress.org/xpol555
License: GPL2
*/


class codeExamplesPlugin {
	const plugin_version = "0.1";
	const CDN_JS_css_path = "//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/";
	const CDN_JS_js = "//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/highlight.min.js";
	const plugin_name = 'Code Examples Plugin';
	const plugin_slug = 'codexa';
	const codexa_color_scheme_option_key = 'codexa_colorscheme_filename';
	public $cssFile;

	function __construct() {
		$this->init();
		$this->adminInit();

	}


	public function init() {
		$this->cssFile = get_option( self::codexa_color_scheme_option_key, 'monokai-sublime.css' );
		$this->addFrontendFiles();
	}
    private function getCodexaCssUrl(){
	    return self::CDN_JS_css_path . str_replace( '.css', '.min.css', $this->cssFile );
    }
	public function getScriptToHead() {
		echo '<link rel="stylesheet" href="' . $this->getCodexaCssUrl() . '">
				<script src="' . self::CDN_JS_js . '"></script>';
	}

	public function getPluginFrontendJavascript() {
		?>
        <script type="text/javascript">
            var codexa;
            /**
             * The main Class of the Code Examples Plugin
             * @type {CodeExamplePluginJavascript}
             */
            codexa = class CodeExamplePluginJavascript {

                static checkHighLightJsExists() {
                    var response = false;
                    if (typeof window.hljs !== 'undefined') {
                        response = true;
                    }
                    return response;
                }

                static init() {
                    if (this.checkHighLightJsExists()) {
                        jQuery(document).ready(function ($) {
                            $(this).find('pre code').each(function (i, block) {
                                $('body').addClass('has-codexa');
                                hljs.highlightBlock(block);
                                $(this).closest('pre').addClass('codexa-inited');
                            });
                        });
                    }
                }
            };
            codexa.init();
        </script>
		<?php
	}

	public function getPluginFrontendInlineCss() {
		?>
        <style>
            pre.codexa-inited {
                padding: 0;
                border: 0;
            }

            .codexa-inited code {
                padding: 15px;
            }
        </style>
		<?php
	}

	private function addFrontendFiles() {
		add_action( 'wp_head', array( $this, 'getScriptToHead' ) );
		add_action( 'wp_head', array( $this, 'getPluginFrontendInlineCss' ) );
		add_action( 'wp_footer', array( $this, 'getPluginFrontendJavascript' ) );
	}

	public function adminInit() {
		add_action( 'admin_menu', array( $this, 'adminMenu' ) );

	}

	public function adminMenu() {
		add_options_page( self::plugin_name, self::plugin_name, 'activate_plugins', self::plugin_slug, array(
			$this,
			'codexaAdminContent'
		) );
	}

	private function internalCurl( $url ) {
        $res = "";
        if(function_exists('curl_version')){
	        $ch = curl_init();

	        // set URL and other appropriate options
	        curl_setopt( $ch, CURLOPT_URL, $url );
	        //curl_setopt( $ch, CURLOPT_HEADER, 0 );
	        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

	        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	        curl_setopt( $ch, CURLOPT_VERBOSE, true );
	        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	        curl_setopt( $ch, CURLOPT_USERAGENT, $agent );
	        // grab URL and pass it to the browser
	        $res = curl_exec( $ch );
	        // close cURL resource, and free up system resources
	        curl_close( $ch );
        }else{
            $file = dirname(__FILE__).'/assets/list.json';
            if(file_exists($file)){
	           $res = file_get_contents($file);
            }
        }
		return $res;
	}

	private function getAvailableStyles() {
		$styles           = array();
		$remoteGithubFile = $this->internalCurl( 'https://api.github.com/repos/isagalaev/highlight.js/git/trees/cff171ed0ce2b240c3beda73b854effe818b4d95' );
		$data             = json_decode( $remoteGithubFile );
		if ( $data ) {
			$files = $data->tree;
			foreach ( $files as $file ) {
				$styles[] = $file->path;
			}
		}

		return $styles;
	}

	public function codexaAdminContent() {
		$styles = $this->getAvailableStyles();
		if ( isset( $_POST['codexa_css_file'] ) && in_array( $_POST['codexa_css_file'], $styles ) ) {
			$this->cssFile = $_POST['codexa_css_file'];
			update_option( self::codexa_color_scheme_option_key, $this->cssFile );
		}
		?>
        <div class="wrap">
            <h1><?php echo self::plugin_name; ?></h1>
            <div class="postbox ">
                <div class="inside">
                    <div class="r">
                        <div id="leftcol">
                            <h3>Color Scheme:</h3>
                            <form action="" method="POST">
                                <small><em>Use arrows to switch themes for preview</em></small>
                                <select name="codexa_css_file">
			                        <?php foreach ( $styles as $style ): ?>
                                        <option value="<?php echo $style; ?>" <?= ( $this->cssFile == $style ) ? 'selected="selected"' : '' ?>><?php echo $style; ?></option>
			                        <?php endforeach; ?>
                                </select>
		                        <?php submit_button(); ?>
                            </form>
                        </div>
                        <div id="rightcol">
                            <div class="r">
                                <div class="halfcol"><textarea style="min-height: 200px;" placeholder="Try some code here..." id="example_code" class="widefat"></textarea>
                                </div>
                                <div class="halfcol" id="preview_col">
                                    <small><span class="dashicons dashicons-info"></span> <em><bold>Note: </bold> This preview might differ from your frontend.</em></small>
                                    <pre>
                                        <code>

                                        </code>
                                    </pre>

                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .r {
                            display: table;
                            width: 100%;
                        }

                        div#leftcol {
                            width: 15%;
                            display: table-cell;
                            vertical-align: middle;
                        }

                        div#rightcol {display: table-cell;vertical-align: middle;}

                        .halfcol {
                            float: left;
                            width: 50%;
                            padding: 15px;
                            box-sizing: border-box;
                        }

                        textarea#example_code {
                            font-family: monospace;
                        }

                        code {
                            width: 100%;
                            display: block;
                        }
                    </style>
                    <link rel="stylesheet" type="text/css" id="codexa_preview_css_file" href="<?=$this->getCodexaCssUrl()?>">
                    <script type="text/javascript" src="<?php echo self::CDN_JS_js;?>"></script>
                    <script type="text/javascript">
                        jQuery(document).ready(function($){
                            $('.wrap').on('input',function(){
                                var code = $('#example_code').val();
                                $('#rightcol pre > code').detach();
                                $('#preview_col > pre').append('<code></code>');
                                $('#rightcol').find('pre > code').text(code);
                                var base = "<?=self::CDN_JS_css_path?>";
                                var file = $('[name="codexa_css_file"]').find(':selected').text();
                                file = file.replace('.css','.min.css');
                                $('#codexa_preview_css_file').attr('href',base+file);

                                $(this).find('pre code').each(function (i, block) {
                                    hljs.highlightBlock(block);
                                });
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
		<?php
	}
}


$CodExa = new codeExamplesPlugin();


