=== CODEXA: Syntax Highlight for Code Examples Snippets ===
Contributors: xpol555
Tags: code examples,snippets,code,syntax highlight,syntax highlighting,syntax,highlight,text,format,backend,settings,frontend,customization,misc,miscellanuos
Donate link: http://paypalme/paolofalomo
Requires at least: 3.5.6
Tested up to: 4.7.4
Stable tag: 4.7.4
License: GPL2

Enable code syntax highlight on your website.

== Description ==
Wordpress plugin for [highlightjs](https://highlightjs.org/)